//#########################################################
//###                    Функции                        ###
//#########################################################
// банальный blink
#ifdef  TEST_BLINK
    void blink () {
      digitalWrite(LED_BUILTIN, LOW); 
      delay(300);
      digitalWrite(LED_BUILTIN, HIGH); 
    }
#endif

// установка WIFI соединения
void setup_wifi() {
    delay(10);
    // We start by connecting to a WiFi network
    /*
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        #ifdef SERIAL_DEBUG
          Serial.print(".");
        #endif
    }
    */
    while ( !connected )  {													                // пока не подключилилсь исполняем код
      for ( counter; counter <= wifi_counter; counter++ )  {				// по почереди перебираем ssid/pass, что определены в массиве авторизации
        uint8_t ssid     = 0+counter*5;           // перебираем ssid
        uint8_t password = 1+counter*5;           // перебираем passwd
        #ifdef SERIAL_DEBUG
          Serial.println();
          Serial.print("Try connect to ");
          Serial.println(wm[ssid]);
        #endif
        WiFi.begin(wm[ssid], wm[password]);                             // коннектимся с выбранными ssid passwd
        while ( WiFi.status() != WL_CONNECTED && connect_counter ) {		// если не подключились или закончилось количество попыток подключения
              delay(500);
              #ifdef SERIAL_DEBUG
                Serial.print(".");
              #endif
              connect_counter--;                  // уменьщаем количесво попыток подключения в цикле
          }
        // проверяем вышли из while из-за счетчика попыток подключения или подключились к WiFi
        if ( !connect_counter )  {                // если из-за счетчика, то обновляем число попыток
            connect_counter = 20;
            #ifdef SERIAL_DEBUG
                Serial.print("\n WIFI Connection err with SSID  ");
                Serial.println(wm[ssid]);
            #endif
          }else{                                  // если из-за того что подключились
            connected = true;                     // поднимаем флаг о подключении
            #ifdef SERIAL_DEBUG
                Serial.print("\n SUCCESSFULY connect to  ");
                Serial.println(wm[ssid]);
            #endif
            break;
          }

      }
	
}



    #ifdef SERIAL_DEBUG
        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
    #endif
}

// функция подписки на чтение данных из топика MQTT и их обработку
void callback(char* topic, byte* payload, uint16_t length) {  // наверняка хватило бы и uint8_t, так же связано с переменной i
  char mqttString[length];             // буфер, объявленный локально для получения строки из mqtt; @ было [length+1] хз зачем, может для добавления в конце "\0"

  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.print("Message arrived in topic [");
    Serial.print(topic);
    Serial.print("] ");
    Serial.print("Message: ");
  #endif

  // функция для конвертации сообщения из MQTT
  for (uint16_t i = 0; i < length; i++) {
    mqttString[i] = (char)payload[i];  // переводим посимвольный вывод из MQTT в строку для последующей обработки
    #ifdef SERIAL_DEBUG
      //Serial.print((char)payload[i]);
      Serial.print(mqttString[i]);
    #endif
  }

  // если сообщение в сервисном топике, то...  (Чтение EEPROM, стирание EEPROM, BIND)
  if(strcmp(topic, MQTT_service_IN) == 0)  {
    #ifdef  TEST_BLINK
        blink (); // моргаем для теста
    #endif

    // сообщаем ардуине комманду - read eeprom, в последствии надо в MQTT ловить вывод данных от Arduino
    if (strcmp(mqttString, MQTT_EPPROM_READ) == 0)  { 
        Serial.println(EPPROM_READ);   // сообщаем Ардуине, что надо прочесть eeprom
    }  

    // сообщаем ардуине комманду - eeprom clear
    if (strcmp(mqttString, MQTT_EPPROM_CLEAR) == 0)  { 
        Serial.println(EPPROM_CLEAR);   // сообщаем Ардуине, что надо стереть eeprom
    }

    // если получаем BIND00, то сообщаем ардуине, что хотим забиндится как кнопка управления реле 0, запись в ячейку eeprom №0
    // !! @ нет проверки на корректность длинны команды
    if ( (mqttString[0] == 'B') && (mqttString[1] == 'I') && (mqttString[2] == 'N') && (mqttString[3] == 'D') )  { 
        Serial.print(KEY_BIND); Serial.print(mqttString[4]); Serial.println(mqttString[5]);   // сообщаем Ардуине, что надо биндить ключ на реле 0, записать в ячейку Х eeprom
    }

  }

  // поморгать свтодиодом по сообщению из MQTT, только если тема топика переключение реле - MQTT_relay_0_IN
  if(strcmp(topic, MQTT_relay_0_IN) == 0)  {
    //if (mqttString[0] == '1') {digitalWrite(LED_BUILTIN, LOW);  Serial.println(); Serial.print("MQTT resciev  "); Serial.println(mqttString[0]);}
    //if (mqttString[0] == '0') {digitalWrite(LED_BUILTIN, HIGH); Serial.println(); Serial.print("MQTT resciev  "); Serial.println(mqttString[0]);}
    if (mqttString[0] == '1')  Serial.println("rel01");
    if (mqttString[0] == '0')  Serial.println("rel00");
  }

  if(strcmp(topic, MQTT_relay_1_IN) == 0)  {
    //if (mqttString[0] == '1') {digitalWrite(LED_BUILTIN, LOW);  Serial.println(); Serial.print("MQTT resciev  "); Serial.println(mqttString[0]);}
    //if (mqttString[0] == '0') {digitalWrite(LED_BUILTIN, HIGH); Serial.println(); Serial.print("MQTT resciev  "); Serial.println(mqttString[0]);}
    if (mqttString[0] == '1')  Serial.println("rel11");
    if (mqttString[0] == '0')  Serial.println("rel10");
  }

  // Здесь необходимо расписать действия по получению данных из MQTT, возможно из этой функции можно будет вернуть глобальную переменную

  #ifdef SERIAL_DEBUG
    Serial.println();
    Serial.println("-----------------------");
  #endif
}

// функция переподключения к WiFi и к mqtt
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    #ifdef SERIAL_DEBUG
      Serial.print("Attempting MQTT connection...");
    #endif
    // Create a random client ID
    // Этот кусок я пролюбил в основном проекте, надо или самому назначать уникальные имена клиентов или оставить этот кусок кода и он будет генерить рандом
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    //if (client.connect("ESP8266Client")) {
    // авторизация mqtt
    //if ( client.connect( clientId.c_str(), mqtt_user, mqtt_pass ) ) {
    uint8_t mqtt_user = 3+counter*5;
    uint8_t mqtt_pass = 4+counter*5;
    if ( client.connect( clientId.c_str(), wm[mqtt_user], wm[mqtt_pass] ) ) {
      #ifdef SERIAL_DEBUG
        Serial.println("MQTT connected");
      #endif
      // Once connected, publish an announcement...
      #ifdef TEST_PUB
        //client.publish("ESP_test/outTopic", "ESP connected!");
        client.publish(MQTT_outTOPIC, "ESP connected!");
      #endif
      // ... and resubscribe
      client.subscribe(MQTT_relay_0_IN);      // подписка/переподписка на топик, входящих статусов для реле #0
      client.subscribe(MQTT_relay_1_IN);      // подписка/переподписка на топик, входящих статусов для реле #0
      client.subscribe(MQTT_service_IN);      // подписка/переподписка на топик, где ожидаем сервисные входящие сообщения
      /*
      //Подписка на 5 топиков возможна......
      //Подписка более, чем на 5 топиков роняет соединение к MQTT брокеру.
      client.subscribe("36/Output/01");
      client.subscribe("36/Output/02");
      client.subscribe("36/Output/03");
      client.subscribe("36/Output/04");
      client.subscribe("36/Output/05");
      */
    } else {
      #ifdef SERIAL_DEBUG
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
      #endif
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


//========================================
//  @
//  // Спётро - http://forum.arduino.cc/index.php?topic=288234.0
//  В текущей реализации эти 2 функции шлют все что получили из UART 
//  в один единственный топик MQTT
//  Переделать!
//========================================

// функция отправки чего-либо в MQTT
void sendMQTT(){
    // @переработать, т.к. отдает любые данные в UART, даже тестовые. 
    /*if (newData == true) {            // если у нас есть новые данные для публикации в MQTT
      client.publish(MQTT_outTOPIC, receivedChars); // publish: arduino -> mqtt bus
      newData = false;                // снимаем флаг
    }*/

    #ifdef TEST_PUB   // тестовая публикация данных в mqtt-топик
        if (now - lastMsg < 0 ) lastMsg = 0;  // при переполнении millis() сработает и сбросит lastMsg, который будет обчень большим. @ можно проверить только через 50 дней работы
        if ( (now - lastMsg > MQTT_TIMEOUT))  {   // аналог задержки в 20с, но более умный способ, т.к. loop продолжает крутится и если мы отслеживаем наличие изменений в mqtt или еще где, то это отличный способ
            lastMsg = now;
            ++value;
            snprintf (msg, 75, "Hello Mike #%d", value);
            #ifdef SERIAL_DEBUG
              Serial.print("Publish message: ");
              Serial.println(msg);
            #endif  
            //client.publish("ESP_test/outTopic", msg);
            client.publish(MQTT_outTOPIC, msg);
        }
    #endif
}

// функция чтения данных из UART до символа '\n'(перевод строки) и запись их в массив receivedChars[]
void recvWithEndMarker() {
  static byte ndx = 0;
  char endMarker = '\n';
  char rc;
 
  while (Serial.available() > 0 && newData == false) {    // пока есть данные в UART и висит флаг о наличии новых данных
    rc = Serial.read();                                   // читаем в rc побайно

    if (rc != endMarker) {                                // пока не "словили" символ конца строки
      receivedChars[ndx] = rc;                            // записываем в массив
      ndx++;
      if (ndx >= numChars) {                              // если размер сообщения больше, чем заданный размер(numChars) массива, то перезаписываем последний элемент массива
        ndx = numChars - 1;
      }
    }
    else {
      receivedChars[ndx] = '\0';                          // последний элемент будет '\0'
      ndx = 0;                                            // обнуляем счетчик массива
      newData = true;                                     // поднимаем флаг наличия новых данных
    }
  }
}

// парсим данные, что пришли по UART, например от arduino
void uartParce()  {
  recvWithEndMarker();
  // @ попробовать парсить в функции recvWithEndMarker в отдельный массив ключ "rel" и в отдельный один массив номер реле и значение "0" или "1" - т.е. 2 элемента в массиве
  if ( newData && (receivedChars[0] == 'r') && (receivedChars[1] == 'e') &&  (receivedChars[2] == 'l') )  {

      // реле №0
      if (receivedChars[3] == '0')  {
          if (receivedChars[4] == '0') client.publish(MQTT_relay_0_OUT, "0");
          if (receivedChars[4] == '1') client.publish(MQTT_relay_0_OUT, "1");
          receivedChars[3] = '\0';      // это сброс 4 элемента массива, что бы более не срабатывло условие на номер relay
          receivedChars[0] = '\0';  // это сброс нулевого элемента массива, что бы более не срабатывло ни одно условие
      }

      // реле №1
      if (receivedChars[3] == '1')  { 
          if (receivedChars[4] == '0') client.publish(MQTT_relay_1_OUT, "0");
          if (receivedChars[4] == '1') client.publish(MQTT_relay_1_OUT, "1");
          receivedChars[3] = '\0';      // это сброс 4 элемента массива, что бы более не срабатывло условие на номер relay
          receivedChars[0] = '\0';  // это сброс нулевого элемента массива, что бы более не срабатывло ни одно условие
      }

      newData = false;          // @ вероятно верхнее условие теперь избыточно
  }

  // если есть данные в UART, но это не реле, то считаем, что это сервисные сообщения и кидаем в соответсвующий топик
  if ( newData /*&& !(receivedChars[0] == 'r')*/ ) {          // @ потенциальна проблема, т.к. первая буква может быть 'r'
      client.publish(MQTT_service_OUT, receivedChars);   // publish: arduino -> mqtt bus
      newData = false;                                   // снимаем флаг
  }
}