//#########################################################
//###                     INCLUDE                       ###
//#########################################################
#pragma once
#include <Arduino.h>


//#########################################################
//###                     DEFINE                        ###
//#########################################################
//#define SERIAL_DEBUG                          // выводим в UART отладочные сообщения
//#define WIFI_WORK                             // параметры подключения к сети и MQTT
//#define WIFI_HOME
//#define WIFI_MIKE         
//#define TEST_PUB                                      // публиковать ли в MQTT тестовые сообщения
#define MQTT_TIMEOUT 20000                              // через какие промежутки времени(значение в мс) что-либо будет публиковаться в MQTT
#define TEST_BLINK
#ifdef  TEST_BLINK
        #define LED_BUILTIN 2                                   // явно объявляем встроенный LED для платы lolin, т.к. в PlatformiO указан не тот пин
#endif

#define MQTT_relay_0_IN     "ESP_switch2/relay_0_IN"       // статус реле #0, что получаем из MQTT и отдаем в Arduino
#define MQTT_relay_0_OUT    "ESP_switch2/relay_0_OUT"      // статус реле #0, что постим в MQTT, получая от Arduino
#define MQTT_relay_1_IN     "ESP_switch2/relay_1_IN"       // статус реле #0, что получаем из MQTT и отдаем в Arduino
#define MQTT_relay_1_OUT    "ESP_switch2/relay_1_OUT"      // статус реле #0, что постим в MQTT, получая от Arduino

#define MQTT_service_IN     "ESP_switch2/service_IN"       // сервисные/управляющие данные, что получаем из MQTT(для управления биндингом и пр.)
#define MQTT_service_OUT    "ESP_switch2/service_OUT"      // данные, что отдаем в MQTT(результат выполнения сервисных запросов)

// сокращения управляющих комманд, что приходят от MQTT
#define MQTT_EPPROM_READ         "READ"
#define MQTT_EPPROM_CLEAR        "EEPclr"
// только в коде                  BIND??

// сокращения управляющих комманд, что отдаются в Arduino по UART
#define EPPROM_READ         "read"
#define EPPROM_CLEAR        "cler"
#define KEY_BIND            "bn"


//#########################################################
//###                   VARIABLES                       ###
//#########################################################

// связано с UART
const byte numChars = 32;       // размер массива под сообщение из UART в MQTT
char receivedChars[numChars];   // массив куда будут сложены данные из UART для публикации в MQTT
bool newData;                   // флаг наличия новых данных в UART

// для задержки через millis
long now = 0;                   // для посчета millis
long lastMsg = 0;               

// переменные для тестового вывода в тестовый топик
#ifdef TEST_PUB
    uint16_t value;                 // счетчик сообщений для вывода в рамках TEST_PUB
    char msg[200];
#endif


uint8_t counter=0;              // счетчик строк массива с данными для подключения
uint8_t connect_counter = 20;   // счётчик количества подключений к одной точке
uint8_t wifi_counter = 4;       // счётчик числа объявленных точек wifi !!! обязательно заполнить руками !!!
bool connected = false;         // флаг о состоявшемся подключении к WiFi