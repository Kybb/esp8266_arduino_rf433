//файл-пример того как должен быть заполнен pass.h

//#########################################################
//###                     INCLUDE                       ###
//#########################################################
#pragma once
#include <Arduino.h>


//#########################################################
//###                   VARIABLES                       ###
//#########################################################
// старый способ прямого указания авторизационных данных
#ifdef WIFI_WORK
  const char* ssid = "222";
  const char* password = "12345678P";
  const char* mqtt_server = "test.noip.me";
  const char* mqtt_user = "esp"; // Логи от сервер
  const char* mqtt_pass = "esp"; // Пароль от сервера
  #define MQTT_PORT  38810
#endif

#ifdef WIFI_HOME
  const char* ssid = "111";
  const char* password = "111";
  const char* mqtt_server = "192.168.1.111";
  const char* mqtt_user = "111"; // Логин от сервер
  const char* mqtt_pass = "111"; // Пароль от сервера
  #define MQTT_PORT  1883
#endif

#ifdef WIFI_MIKE
  const char* ssid = "MIKE";
  const char* password = "mike123";
  const char* mqtt_server = "mike.berg.sh";
  const char* mqtt_user = "esp"; // Логи от сервер
  const char* mqtt_pass = "esp"; // Пароль от сервера
  #define MQTT_PORT  18830
#endif

#ifdef WIFI_MIKE_PHONE
  const char* ssid = "Redmi 4X";
  const char* password = "1234567890";
  const char* mqtt_server = "mike.berg.sh";
  const char* mqtt_user = "esp"; // Логи от сервер
  const char* mqtt_pass = "esp"; // Пароль от сервера
  #define MQTT_PORT  18830
#endif

// новый способ(мне меньше нравится) с указанием данных для подключения перебором, учитывается связка известных WiFi и сервера mqtt
// тут важно, что записей о известных сетях должно быть 4, если другое число, том енять в коде.
const char* wm[]= { "Redmi 4X",       "1234567890",     "mike.berg.sh",   "esp",     "esp",
                    "MIKE",           "mike123",        "mike.berg.sh",   "esp",     "esp",
                    "111",            "111",            "192.168.1.111",  "111",     "111",
                    "222",            "12345678P",      "test.noip.me",   "esp",     "esp"
                    
};

uint16_t mqttPort[]= { 18830,
                       38810,
                       1883,
                       18830                      
};